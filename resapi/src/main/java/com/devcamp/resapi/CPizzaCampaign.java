package com.devcamp.resapi;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class CPizzaCampaign {
    @CrossOrigin
    @GetMapping("/devcamp-date")
    public String getDateViet(){
        DateTimeFormatter dtfVietnam = DateTimeFormatter.ofPattern("EEEE").localizedBy(Locale.forLanguageTag("vi"));
    LocalDate today = LocalDate.now(ZoneId.systemDefault());

        return String .format("Hello pizza lover! Hôm nay %s, mua 1 tặng 1", dtfVietnam.format(today));
    }

    }
